# -*- coding: utf-8 -*-

from selenium import webdriver
import sys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.actions.pointer_actions import PointerActions
import time

driver = webdriver.Chrome()
# browser.get('https://ssl.biglion.ru/checkout/order/puteshestvie-navozdushnom-share1-51/5144017/')
driver.get(sys.argv[1])

actions = PointerActions()
wait = WebDriverWait(driver, 10)
wait5 = WebDriverWait(driver, 5)

link = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'sm-goods_main_details_buy-straight')))
driver.execute_script("arguments[0].scrollIntoView()", link)
driver.execute_script("arguments[0].click();", link)
time.sleep(5)
print("loading basket")
driver.get("https://www.sportmaster.ru/basket/checkout.do")
link = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'sm-helping-dialog-button')))
driver.execute_script("arguments[0].scrollIntoView()", link)
driver.execute_script("arguments[0].click();", link)

valid_promocodes = open("sportmaster_valid_promocodes.txt",'w')
invalid_promocodes = open("sportmaster_invalid_promocodes.txt",'w')

promocodes = [line.strip() for line in open('sportmaster_promocodes.txt', "r")]		
count = 0
for promocode in promocodes:
	count = count + 1
	print("processing promocode %s (%s from %s)" % (promocode, count, len(promocodes)))
	editor = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'newPromo-input')))
	editor.send_keys(promocode)
	link = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'newPromo-apply')))
	driver.execute_script("arguments[0].scrollIntoView()", link)
	driver.execute_script("arguments[0].click();", link)
	success = False
	try:
		element = wait5.until(EC.visibility_of_element_located((By.CLASS_NAME, 'sm-basket__overall-table-promo')))
		success = True
	except Exception, e:
		pass
	if success:
		print("promocode %s is VALID" % promocode)
		valid_promocodes.write(promocode)
		valid_promocodes.write("\n")
		link = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'promoApplied-remove')))
		driver.execute_script("arguments[0].scrollIntoView()", link)
		driver.execute_script("arguments[0].click();", link)
	else:
		print("promocode %s is INVALID" % promocode)
		invalid_promocodes.write(promocode)
		invalid_promocodes.write("\n")
		link = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@data-selenium="close_button"]')))
		driver.execute_script("arguments[0].scrollIntoView()", link)
		driver.execute_script("arguments[0].click();", link)
		link = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'promoApplied-remove')))
		driver.execute_script("arguments[0].scrollIntoView()", link)
		driver.execute_script("arguments[0].click();", link)
valid_promocodes.close()
invalid_promocodes.close()			
print("all completed")
driver.quit()